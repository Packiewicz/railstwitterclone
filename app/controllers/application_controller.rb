class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  helper_method :current_user
  helper_method :is_liked

  def is_liked(post)
    if post.nil? || current_user.nil?
      return false
    else
      like = Like.find_by user_id: current_user.id, message_id: post.id
      unless like.nil?
        return true
      end
    end
    return false
  end

  def current_user
    # this-> ||= is something with memoization
    # its used to not hit database everytime we want to access current_user
    # a ||= b (double pipe equals OR or-equals) sets a with b, if a is falsey or undefined
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def authorize
    if current_user.nil?
      redirect_to login_path, alert: "You must be logged in to access this page."
      return false
    end
    return true
  end
end
