class UsersController < ApplicationController

  def index
    u = User.find_by_username(params[:name])
    unless u.nil?
      @user = u
    else
      render :error
    end
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)

    @user.email.downcase!

    if @user.save
      flash.notice = "Added user"
      redirect_to root_path
    else
      flash.alert = @user.errors.messages.to_s
      render :new
    end
  end

  def user_params
    params.require(:user).permit(:username, :email, :password, :password_confirmation)
  end

end
