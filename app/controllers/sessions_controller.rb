class SessionsController < ApplicationController
  def new
  end
  
  def create
    user = User.find_by_username(params[:login][:username])
    if user && user.authenticate(params[:login][:password])
      session[:user_id] = user.id.to_s
      redirect_to root_path, notice: "Logged in"
    else
      flash.alert = "Wrong username or password"
      render :new
    end
  end

  def destroy
      session.delete(:user_id)
      redirect_to login_path, notice: "Logged out"
  end
end
