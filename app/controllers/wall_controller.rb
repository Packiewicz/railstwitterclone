class WallController < ApplicationController
  def index
    @messages = Message.all
  end

  def create
    authorize or return
    @message = Message.create(author: current_user, message: params[:message][:message])
    redirect_to wall_index_path
  end

  def like
    authorize or return
    m = Message.find_by_id(params[:post_id])
    unless m.nil?
      Like.create message_id: params[:post_id], user_id: current_user.id
    end
    redirect_to wall_index_path
  end

  def unlike
    authorize or return
    m = Message.find_by_id(params[:post_id])
    unless m.nil?
      l = Like.find_by message_id: params[:post_id], user_id: current_user.id
      unless l.nil?
        l.destroy
      end
    end
    redirect_to wall_index_path
  end

end
