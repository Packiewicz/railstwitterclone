class Message < ApplicationRecord
  validates :message, presence: true, length: { maximum: 140 }

  belongs_to :author, :class_name => "User", foreign_key: "user_id"
  has_many :likes
  has_many :users, :through => :likes
end
