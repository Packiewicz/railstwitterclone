Rails.application.routes.draw do
  get 'wall/index'
  post 'wall/create'
  post 'wall/like/:post_id', to: 'wall#like', as: 'like'
  post 'wall/unlike/:post_id', to: 'wall#unlike', as: 'unlike'

  get 'profile/:name', to: 'users#index', as: 'profile'
  get 'signup', to: 'users#new'
  post 'users' => 'users#create'

  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'

  root 'wall#index'
end
